from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from FTagDumper.trackUtil import applyTrackSys
from JetTagCalibration.JetTagCalibConfig import JetTagCalibCfg
from BTagging.BTagConfig import RetagRenameInputContainerCfg
from BTagging.BTagTrackAugmenterAlgConfig import BTagTrackAugmenterAlgCfg
from ParticleJetTools.JetParticleAssociationAlgConfig import JetParticleFixedConeAssociationAlgCfg


def GhostAssociationCfg(name, JetCollection, OutputParticleDecoration, GetCorrectedLinks):
    ca = ComponentAccumulator()
    
    ca.addEventAlgo(CompFactory.GhostAssociationSystAlg(
        name=name,
        JetCollection=JetCollection,
        InGhostTracks=f'{JetCollection}.GhostTrack',
        OutGhostTracks=f'{JetCollection}.{OutputParticleDecoration}',
        GetCorrectedLinks=GetCorrectedLinks,
        )
    )
    return ca
    

def AddTrackSys_largeR(
    flags,
    track_collection="InDetTrackParticles",
    sys_list=[],
    jet_collection="AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets",    
):

    ca = ComponentAccumulator()
    track_collection="InDetTrackParticles"
    jet_collection="AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets"
    
    print('APPLYING SYSTEMATIC:', sys_list)
    if sys_list:
        output_track_collection = "InDetTrackParticles_Sys"
        ca.merge(applyTrackSys(flags, sys_list, track_collection , jet_collection, output_track_collection))
        track_collection = output_track_collection


    
    ca.merge(BTagTrackAugmenterAlgCfg(
            flags,
            TrackCollection=track_collection,
            PrimaryVertexCollectionName='PrimaryVertices',
        ))
  
    
    jet_collection_nosuffix = "AntiKt10UFOCSSKSoftDropBeta100Zcut10"
    addRenameMaps=['xAOD::TrackParticleAuxContainer#' + track_collection + '.btagIp_invalidIp->' + track_collection + '.btagIp_invalidIp_' + '_retag']
    ca.merge(RetagRenameInputContainerCfg("_retag", jet_collection_nosuffix, tracksKey=track_collection, addRenameMaps=addRenameMaps))
    ca.merge(JetTagCalibCfg(flags))
    

    fixedConeRadius = 1.0
    ca.merge(JetParticleFixedConeAssociationAlgCfg(
        flags,
        fixedConeRadius,
        JetCollection="AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets",
        InputParticleCollection=track_collection,
        OutputParticleDecoration="DeltaRTrackLink")
    )

    getCorrectedLinks = True if sys_list else False
    ca.merge(GhostAssociationCfg( 
        name="GhostAssociationSyst",
        JetCollection="AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets",
        OutputParticleDecoration="GhostTrackLink",
        GetCorrectedLinks=getCorrectedLinks,
        )
    )        
    
    return ca
