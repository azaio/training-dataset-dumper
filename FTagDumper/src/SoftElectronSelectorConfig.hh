#ifndef SOFT_ELECTRON_SELECTOR_CONFIG_HH
#define SOFT_ELECTRON_SELECTOR_CONFIG_HH

struct SoftElectronSelectorConfig
{
  struct Cuts {
    float pt_minimum;
    float pt_maximum;
    float abs_eta_maximum;
    float d0_maximum;
    float eta_maximum;
    float ptrel_maximum;
    float isopt_maximum;
    float eop_maximum;
    float rhad1_maximum;
    float wstot_maximum;
    float rphi_maximum;
    float reta_maximum;
    float deta1_maximum;
    float dpop_maximum;
  };
  Cuts cuts;
};

#endif
